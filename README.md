# About
Dockerfile containing the requirements to build [mandelbulber2](https://github.com/buddhi1980/mandelbulber2) binaries


# Start
```bash
docker run -it acidhunter/mandelbulber2-docker:[Trusty|Vivid|Xenial|Zesty]
```


## License
Copyright (c) 2017 Martin Friedrich @acidhunter.
Released under the MIT license. See [license](LICENSE.MD)


## Contact
- [Telegram](https://t.me/acidhunter)
- [Twitter](https://twitter.com/Panic_Network)
- [Facebook](https://facebook.com/geist.mit.zeit)
- [Google+](https://plus.google.com/+MartinF)
